package main

import "fmt"

// Simple demo of creating Thunks in Golang.
// Named funcs and anonymous.
func main() {
	thunk := addThunk(3, 4)
	demoTheThunk("This is going to call a thunk constructed from a named function", thunk)
	thunk2 := func(a, b int) func() int {
		return func() int {
			return a + b
		}
	}(4, 4)
	demoTheThunk("This is going to call a thunk constructed anonymously", thunk2)
}

// A method that returns a func() int that has the addends baked in.
func addThunk(a, b int) func() int {
	return func() int {
		return a + b
	}
}

// Demonstrate the working of the Thunk.
func demoTheThunk(msg string, thunk func() int) {
	fmt.Println(msg)
	fmt.Println(thunk())
}
